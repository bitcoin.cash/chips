# 2021-01 Decentralized Cash Improvement Proposals Naming

v0.1

Authors: Tom Zander, after the idea of Griffith and with input from John Nieri

# Problem Statement

We do not yet have a naming convention for improvement proposals for BCH. Thus far everything has been attached to a hard fork or is a BIP. It is easy to keep track of ideas and proposals right now because there are so few. Eventually this will become harder.

For organizational purposes, this CHIP proposed that we adopt CasH Improvement Proposal (CHIP) as the convention of naming improvement proposals for BCH.

# Naming

A decentralized system allows assigning an ID without any central party giving them out. And this implies that its probably not useful to use numbers, or at least not numbers alone.
Some system has to be suggested that lies somewhere between the easy-but-centralized numbering and hard-but-unique idea of doing a sha1 of your document.

Maybe the simplest solution is to have proposals leading with 4 digits for year, 2 for month, and followed with a name of more than 2 and no more than 5 short English words.

Example:

* 2020-10-Wallet Bluetooth Controller
* 2020-12-Holiday Cheer Christmas colors

The amount of words chosen for the title should be 3, 4 or 5 simple English words. The title should be descriptive of the actual proposal and generally speaking the naming should be boring. What that means it that cute, smart or geeky references should be avoided, as the first goal is to create understanding.

# Copyright Notice

Copyright (C)  2020-2021 Tom Zander  
Copyright (C)  2020 Griffith  

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.

